import { PLAY_GAME } from "../constant/xucXacConstant";
let initialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
  tongSoLanChoi: 0,
  tongSoLanThang: 0,
};
export const xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      console.log("yes");

      // tạo mảng xúc xắc mới
      let newMangXucXac = state.mangXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucSac/${random}.png`,
          giaTri: random,
        };
      });
      // tính kết quả
      return { ...state, mangXucXac: newMangXucXac };
    }
    default:
      return state;
  }
};
